import useFetchDataFromApi from "./useFetchDataFromApi";

export default function UseFetchExample() {
  const {
    data: quote,
    loading,
    error,
  } = useFetchDataFromApi("https://api.quotable.io/random");

  return (
    <>
      {loading && <p>{loading}</p>}
      {quote && <p>"{quote}"</p>}
      {error && <p>{error}</p>}
    </>
  );
}
