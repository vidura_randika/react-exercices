import { useEffect, useState } from "react";

export default function UseEffectExampleWithDependencyArray(){
    const [count, setCount] = useState(0);

    useEffect(() => {
      console.log(`You have clicked the button ${count} times`);
    }, [count]);

    return (
      <div>
        <p>Please check the console and click the button. It'll update now.</p>
        <button onClick={() => setCount(count + 1)}>Click me</button>
      </div>
    );
}