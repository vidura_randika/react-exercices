import { useState } from "react";

export default function UseStateExample() {
  const [name, setName] = useState("Randika");

  const updateName = () => {
    setName("Vidura");
  };
  return (
    <>
      <h4>My name is {name}</h4>
      <br />
      <button onClick={updateName}>Change name</button>
    </>
  );
}
