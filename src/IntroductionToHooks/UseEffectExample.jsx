import { useEffect, useState } from "react";

export default function UseEffectExample(){
    const [count, setCount] = useState(0);

    useEffect(() => {
      console.log(`You have clicked the button ${count} times`);
    }, []);

    return (
      <div>
        <p>Please check the console and click the button. It'll not console.log again, because of dependency array is empty.</p>
        <button onClick={() => setCount(count + 1)}>Click me</button>
      </div>
    );
}