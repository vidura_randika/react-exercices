import { BrowserRouter, Routes, Route } from "react-router-dom";
import IfElse from "./ConditionalRendering/IfElse";
import LogicalOperator from "./ConditionalRendering/LogicalOperator";
import SwitchCase from "./ConditionalRendering/SwichCase";
import TernaryOperator from "./ConditionalRendering/TernaryOperator";
import FormValidationYup from "./FormValidationYUP/formValidationYup";
import CreateElementExample from "./CreateElements_Lists_Keys_Ref/CreateElementExample";
import ListAndKeys from "./CreateElements_Lists_Keys_Ref/ListsAndKeys";
import ReactRefExample from "./CreateElements_Lists_Keys_Ref/ReactRefExample";
import { Home } from "./Home";
import UseEffectExample from "./IntroductionToHooks/UseEffectExample";
import UseEffectExampleWithDependencyArray from "./IntroductionToHooks/UseEffectExampleWithDependencyArray";
import UseFetchExample from "./IntroductionToHooks/UseFetchExample";
import UseStateExample from "./IntroductionToHooks/UseStateExample";
import { TodoList } from "./PassingProps/TodoList";
import DisplayClock from "./ReactLifeCycles/DisplayClock";
import UseNavigateAbout from "./UseNavigate/UseNavigateAbout";
import UseNavigationHome from "./UseNavigate/UseNavigateHome";
import SetContextExample from "./ContextExample/SetContextExample";
import Book from "./ReduxExample/Component/Book";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/pass-props" element={<TodoList />} />
          <Route path="/react-lifecycles" element={<DisplayClock />} />
          <Route
            path="/react-conditional-rendering-if-else"
            element={<IfElse />}
          />
          <Route
            path="/react-conditional-rendering-logical-operator"
            element={<LogicalOperator />}
          />
          <Route
            path="/react-conditional-rendering-ternary-operator"
            element={<TernaryOperator />}
          />
          <Route
            path="/react-conditional-rendering-switch-case"
            element={<SwitchCase />}
          />
          <Route
            path="/react-useNavigation-home"
            element={<UseNavigationHome />}
          />
          <Route
            path="/react-useNavigation-about"
            element={<UseNavigateAbout />}
          />
          <Route
            path="/form-validation-yup-schema"
            element={<FormValidationYup />}
          />
          <Route path="/react-state-example" element={<UseStateExample />} />
          <Route
            path="/react-useEffect-example"
            element={<UseEffectExample />}
          />
          <Route
            path="/react-useEffect-example-with-dependency-array"
            element={<UseEffectExampleWithDependencyArray />}
          />
          <Route path="/react-useFetch-example" element={<UseFetchExample />} />
          <Route
            path="/react-createElement-example"
            element={<CreateElementExample />}
          />
          <Route path="/react-lists-and-keys" element={<ListAndKeys />} />
          <Route path="/react-ref-example" element={<ReactRefExample />} />
          <Route path="/react-context-example" element={<SetContextExample />} />
          <Route path="/react-redux-example" element={<Book />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
