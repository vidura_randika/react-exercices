import React, { Component } from "react";
import "../App.css";

class Clock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dateAndTime: new Date(),
      count: 0,
    };
  }

  componentDidMount() {
    setInterval(() => {
      this.updateTime();
    }, 1000);
  }

  componentDidUpdate(previousProps, previousState, snapshot) {
    if (this.state.dateAndTime !== previousState.dateAndTime) {
      this.setState({ count: this.state.count + 1 });
    }
  }

  componentWillUnmount(){
    alert("Clock component will unmount");
  }

  updateTime = () => {
    this.setState({
      dateAndTime: new Date(),
    });
  };
  render() {
    return (
      <div className="App">
        <h1>React life cycles</h1>
        <br />
        <h2>Now time is : {this.state.dateAndTime.toLocaleTimeString()}</h2>
        <h3>This component is updated {this.state.count} times.</h3>
      </div>
    );
  }
}

export default Clock;
