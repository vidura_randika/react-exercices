import React, { Component } from "react";
import Clock from "./Clock";

class DisplayClock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
    };
  }

  unmountClock=()=>{
    this.setState({
        show:false
    })
  }

  render() {
    return (
      <div>
        {this.state.show === true ? <Clock /> : <h2>Clock unmounted</h2>}
        <br />
        <button onClick={this.unmountClock}>Unmount clock</button>
      </div>
    );
  }
}

export default DisplayClock;
