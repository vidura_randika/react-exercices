import React from "react";

export default function CreateElementExample() {
  const customHeading = React.createElement(
    "h1",
    { style: { color: "red" } },
    "This is example for react create element"
  );

  return customHeading;
}
