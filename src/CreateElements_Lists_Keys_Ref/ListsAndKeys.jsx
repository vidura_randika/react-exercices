export default function ListAndKeys() {
  const vehicleTypes = [
    "Car",
    "Consumer Trucks",
    "Semi-Truck",
    "Aerosani",
    "Golf Car",
    "Bus / Coach",
    "Tractor",
    "Sports Car",
    "Motorcycle / Scooter",
    "Unicycle",
    "Bicycle",
    "Tricycle",
    "Kick Scooter",
    "Land Yacht",
  ];

  return (
    <>
      <h1>List with keys</h1>
      <br />
      <h3>Vehicle Types</h3>
      <ul>
        {vehicleTypes.map((type, index) => (
          <li key={index}>{type}</li>
        ))}
      </ul>
    </>
  );
}
