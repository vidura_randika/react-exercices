import { useRef } from "react";

export default function ReactRefExample() {
    const inputRef = useRef()

    const viewInConsole = () => {
        console.log(inputRef.current?.value)
    }

    return (
        <>
            <input ref={inputRef}/>
            <br />
            <button onClick={viewInConsole}>View value in console</button>
        </>
    )
}
