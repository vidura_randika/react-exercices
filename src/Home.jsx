import "./App.css";
import React from "react";
import { Link } from "react-router-dom";

export function Home() {
  return (
    <div className="App">
      <h1>React exercices</h1>
      <br />
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/pass-props">Passing data through props</Link>
        </li>
        <li>
          <Link to="/react-lifecycles">React life cycle methods</Link>
        </li>
        <hr />
        <br />
        <h5>Conditional rendering</h5>
        <ol>
          <li>
            <Link to="/react-conditional-rendering-if-else">If Else</Link>
          </li>
          <li>
            <Link to="/react-conditional-rendering-logical-operator">
              Logical Operator
            </Link>
          </li>
          <li>
            <Link to="/react-conditional-rendering-ternary-operator">
              Ternary Operator
            </Link>
          </li>
          <li>
            <Link to="/react-conditional-rendering-switch-case">
              Switch case
            </Link>
          </li>
        </ol>
        <br />
        <hr />
        <li>
          <Link to="/react-useNavigation-home">Use Navigation - Home</Link>
        </li>
        <br />
        <hr />
        <li>
          <Link to="/form-validation-yup-schema">
            Form validation - yup schema
          </Link>
        </li>
        <br />
        <hr />
        <h5>React introduction to hooks</h5>
        <ol>
          <li>
            <Link to="/react-state-example">Use state example</Link>
          </li>
          <li>
            <Link to="/react-useEffect-example">Use effect example</Link>
          </li>
          <li>
            <Link to="/react-useEffect-example-with-dependency-array">
              Use effect example with dependency array
            </Link>
          </li>
          <li>
            <Link to="/react-useFetch-example">Use Fetch example</Link>
          </li>
        </ol>
        <br />
        <hr />
        <h5>React createElement, Lists, Keys and React Refs</h5>
        <ol>
          <li>
            <Link to="/react-createElement-example">
              Create element example
            </Link>
          </li>
          <li>
            <Link to="/react-lists-and-keys">React lists and keys</Link>
          </li>
          <li>
            <Link to="/react-ref-example">React ref example</Link>
          </li>
        </ol>
        <br />
        <hr />
        <li>
          <Link to="/react-context-example">React context example</Link>
        </li>
        <br />
        <hr />
        <li>
          <Link to="/react-redux-example">React redux example</Link>
        </li>
      </ul>
    </div>
  );
}
