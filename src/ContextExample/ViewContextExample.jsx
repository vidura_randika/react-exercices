import React, { useContext } from "react";
import ExampleContext from "./ExampleContext";

export default function ViewContextExample() { 
	const data = useContext(ExampleContext);
	return <h1>HELLO !... I'm { data.name}</h1>
}