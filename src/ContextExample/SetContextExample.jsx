import { useRef, useState } from "react";
import ExampleContext from "./ExampleContext";
import ViewContextExample from "./ViewContextExample";

export default function SetContextExample() {
	const nameInput = useRef();

	const [data, setData] = useState({
		name:""
	})

	const changeName = () => {
		setData({name:nameInput.current?.value})
	}

	return (
		<ExampleContext.Provider value={data}>
			<input placeholder="Please input name" ref={nameInput} />
			<br />
			<button onClick={changeName}>Change name</button>
			<ViewContextExample/>
		</ExampleContext.Provider>
	)
}