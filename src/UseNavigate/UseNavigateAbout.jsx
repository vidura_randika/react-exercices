import { useLocation, useNavigate } from "react-router-dom";

export default function UseNavigateAbout(){
    const navigate = useNavigate();
    const location = useLocation();

    //print optional argument passed from the previous page
    console.log(location.state);

    return(
        <>
            <h1>Use Navigate - About</h1>
            <button onClick={()=>navigate(-1)}>Back</button>
        </>
    );
}