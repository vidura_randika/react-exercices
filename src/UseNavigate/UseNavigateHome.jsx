import { useNavigate } from "react-router-dom"

export default function UseNavigationHome(){
    const navigate = useNavigate();

    return (
      <>
        <h1>Use Navigation - Home page</h1>
        <button
          onClick={() =>
            navigate("/react-useNavigation-about", {
              state: { message: "This is optional argument" },
            })
          }
        >
          About
        </button>
      </>
    );
}