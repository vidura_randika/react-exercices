import { useState } from "react"

function UserLoggedIn(){
    return <h1>User is logged in</h1>
}

function Guest(){
    return <h1>Please login</h1>
}

function Display(props){
    if (props.isLoggedIn) {
        return <UserLoggedIn/>
    } else {
        return <Guest/>
    }
}

export default function IfElse(){
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    const changeState = () => {
        setIsLoggedIn(!isLoggedIn);
    }

    return(
        <>
            <button onClick={changeState}>CHANGE STATE</button>
            <br />
            <hr />
            <Display isLoggedIn={isLoggedIn}/>
        </>
    )
}