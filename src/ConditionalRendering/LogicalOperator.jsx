import { useState } from "react";

function UserLoggedIn() {
  return <h1>User is logged in</h1>;
}

export default function LogicalOperator() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [btnName, setBtnName] = useState("Login");

  const changeState = () => {
    if (isLoggedIn) {
        setBtnName("Login");
    } else {
        setBtnName("Logout");
    }
    setIsLoggedIn(!isLoggedIn);
  };
  
  return (
    <>
      <button onClick={changeState}>{btnName}</button>
      {isLoggedIn && <UserLoggedIn />}
    </>
  );
}
