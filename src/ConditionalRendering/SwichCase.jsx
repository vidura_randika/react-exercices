import { useState } from "react";

function UserLoggedIn() {
  return <h1>User is logged in</h1>;
}

export default function SwitchCase() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [btnName, setBtnName] = useState("Login");

  const changeState = () => {
    if (isLoggedIn) {
      setBtnName("Login");
    } else {
      setBtnName("Logout");
    }
    setIsLoggedIn(!isLoggedIn);
  };

  switch (isLoggedIn) {
    case true:
      return (
        <>
          <button onClick={changeState}>{btnName}</button>
          <br />
          <UserLoggedIn />
        </>
      );
    case false:
      return (
        <>
          <button onClick={changeState}>{btnName}</button>
          <br />
          <h1>Please login</h1>
        </>
      );

    default:
      return null;
  }
}
