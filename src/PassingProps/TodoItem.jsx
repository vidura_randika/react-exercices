import { Card, Col, Button, Row } from "react-bootstrap";

export function TodoItem({ todoItem, index, deleteItem }) {
    function handleClick(){
        deleteItem(index);
    }
  return (
    <Card className="m-2">
      <Row>
        <Col>
          <Card.Body>{todoItem}</Card.Body>
        </Col>
        <Col>
          <Button className="m-2" variant="danger" onClick={handleClick}>
            DELETE
          </Button>
        </Col>
      </Row>
    </Card>
  );
}
