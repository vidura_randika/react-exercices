import { TodoItem } from "./TodoItem";
import { Container, Row } from "react-bootstrap";
import { useState } from "react";

export function TodoList() {
  const arr = ["Pay bills", "Read a book", "Organize office"];
  const [todos, setTodos] = useState(arr);

  const deleteItem = (index) => {
    let tempArr = [...todos];
    tempArr.splice(index, 1);
    setTodos(tempArr);
  };

  return (
    <Container>
      <h2 className="m-3">My Todo List</h2>
      <Row>
        {todos.map((item, index) => {
          return (
            <TodoItem
              todoItem={item}
              index={index}
              deleteItem={deleteItem}
              key={index}
            />
          );
        })}
      </Row>
    </Container>
  );
}
